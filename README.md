1. Prepare initial configutation 

- go to "INIT"
- open the kDNA image (called crop.tif)
- create a binar mask w threshold > 65 (estimated using background mean intensity + 3 times its standard deviation)
- save thresholded image as skel.tif
- use image calcuator to "AND" original image and binarised one 
- save as TIFF 
- save as data matrix "matrix_w_intensity.txt"

2. Map matrix of intensities to phantom beads 

- open bash.sh to set the parameters 
- use bas.sh as "./bash.sh" in terminal to create initial configuration 
- in gnuplot  type 
	p "kdna.afm.data" i 0 u 4:5:3 palette
to visualise initial configuration 

3. Run LAMMPS simulation

- go to "RUN"
- create a "parameters.dat" file with random seeds, e.g.
variable seed1 equal 4414
variable seed2 equal 1123
variable seed3 equal 9091
- run as  
	~/lmp_mpi -in run.lam
from terminal
