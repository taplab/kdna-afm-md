
#include<iostream>
#include<math.h>
#include<stdlib.h>
#include<fstream>
#include<sstream>
#include<vector>
#include <unistd.h>
#include<ctime>
using namespace std;

int InitialConfg=0;

int bond1=2;
int ang1=3;
int c=1;
int nb=1;
int na=1;

const int npolymax=5000;
const int nbeadsmax=100;

int bond[npolymax*nbeadsmax][2];
int angle[npolymax*nbeadsmax][3];
double position[npolymax][nbeadsmax][3];
double Rg[npolymax];
double COM[npolymax][3];

int reps;
int nbond=0;
int nangle=0;


int main(int argc, char* argv[]){

//cout << "Input argv: argv[1]:Npoly argv[2]=Nbeads; argv[3]=nrows argv[4]=nmppx argv[5]=afmbeads argv[6]=dsDNAwidth"<<endl;
srand(time(NULL));

int npoly,nbeads;
npoly=atoi(argv[1]);
int ndnabp=atoi(argv[2]);
int nrows=atof(argv[3]);
double nmppx=atof(argv[4]);
int afmbeads=atoi(argv[5]);
double dsDNAwidth=atof(argv[6]);
nbeads=int(ndnabp*0.34/dsDNAwidth);

//
double sigma_per_nm=1.0/dsDNAwidth;

double factor=nmppx*sigma_per_nm;
double L=nrows*factor;

ofstream writeW;
stringstream writeFileW;
writeFileW << "minicircles.afm.data";
writeW.open(writeFileW.str().c_str());
    
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
for(reps=0;reps<npoly;reps++){

//////////////////////////////////////////////////////////////
////////        MAKE POLYMER     /////////////////////
//////////////////////////////////////////////////////////////
//cout << "BONDS" <<endl;
int cumulative=0;
for(int rr=0;rr<reps;rr++)cumulative+=nbeads;
int nbeadT=0;
for(int i=0;i<nbeads;i++){
    nbeadT++;
    //cout << " if " <<endl;
    if(i==nbeads-1){
    bond[nbond][0] = i+cumulative;
    bond[nbond][1] = i+1+cumulative-nbeads;
    }
    else{
    bond[nbond][0] = i+cumulative;
    bond[nbond][1] = i+1+cumulative;
    }
    nbond++;
    //cout << nbond << " " << i << endl;
}
    
for(int i=0;i<nbeads;i++){
    if(i==nbeads-2){
    angle[nangle][0] = i+cumulative;
    angle[nangle][1] = i+1+cumulative;
    angle[nangle][2] = i+2+cumulative-nbeads;
    }
    else if(i==nbeads-1){
    angle[nangle][0] = i+cumulative;
    angle[nangle][1] = i+1+cumulative-nbeads;
    angle[nangle][2] = i+2+cumulative-nbeads;
    }
    else{
    angle[nangle][0] = i+cumulative;
    angle[nangle][1] = i+1+cumulative;
    angle[nangle][2] = i+2+cumulative;
    }
    nangle++;
}

//cout << "MAKING THE POLYMER ..." <<endl;
//UNTIL NOT OVERLAPPING
int overlapping=1;
while(overlapping>=1){
overlapping=0;

///////////////////////////////////////////////////////////
//make polymer
double theta[2];
double phi[2];
double non1=((double)(rand())/((double)(RAND_MAX)));
double non=((double)(rand())/((double)(RAND_MAX)));
    
///////////////////////////////////////////////////////////////////////////
// RING
///////////////////////////////////////////////////////////////////////////

double zshift;
double yshift;
double xshift;
for(int m=0;m<nbeads;m++){
if(m==0){
xshift=(2.0*rand()*1.0/RAND_MAX-1)*3.0*L/8.0;
yshift=(2.0*rand()*1.0/RAND_MAX-1)*3.0*L/8.0;
zshift=0;
//cout << xshift << " " <<yshift <<endl;
}
position[reps][m][0]=nbeads/10.*cos(m*1.0/nbeads*2.0*M_PI)+xshift;
position[reps][m][1]=nbeads/10.*sin(m*1.0/nbeads*2.0*M_PI)+yshift;
position[reps][m][2]=0;
COM[reps][0]+=position[reps][m][0];
COM[reps][1]+=position[reps][m][1];
COM[reps][2]+=position[reps][m][2];
}
COM[reps][0]/=nbeads;
COM[reps][1]/=nbeads;
COM[reps][2]/=nbeads;
//cout << "COM(" <<reps<<")=" <<COM[reps][0] << " " << COM[reps][1] << " " << COM[reps][2] <<endl;

//radius of gyration
for(int m=0;m<nbeads;m++)for(int d=0;d<3;d++) Rg[reps]+=pow(position[reps][m][d]-COM[reps][d],2.0);
Rg[reps]/=nbeads;
//cout << "Rg(" <<reps<<")=" <<sqrt(Rg[reps])<<endl;
cout << "DONE Ring "<< reps << endl;
//check distances with previous
    for (int j=0; j<reps; j++){
        double dx=COM[reps][0]-COM[j][0];
        double dy=COM[reps][1]-COM[j][1];
        double dz=COM[reps][2]-COM[j][2];
        //if(sqrt(dx*dx+dy*dy+dz*dz)<sqrt(Rg[reps]+Rg[j]))overlapping++;
            //cout << "OVERL " << reps << " - " << j << " " << overlapping <<endl;
    }
//cin.get();
    
}//close while overlapping
} //close loop over reps
////////////////////////////////////////////////////
////////        WRITE FILE     /////////////////////
////////////////////////////////////////////////////
int totb=npoly*nbeads;
writeW<< "LAMMPS data file from restart file: timestep = 0,\t procs = 1"<<endl;
writeW << totb+afmbeads << " atoms "<<endl;
writeW << nbond << " bonds "<<endl;
writeW << nangle << " angles "<<endl;
writeW << "\n";
writeW << 10 << " atom types "<<endl;
writeW << 1 << " bond types "<<endl;
writeW << 1 << " angle types "<<endl;
writeW << "\n";
writeW << -L/2.0 << " " << +L/2.0 << " xlo xhi"<<endl;
writeW << -L/2.0 << " " << +L/2.0 << " ylo yhi"<<endl;
writeW << -5 << " " << 5 << " zlo zhi"<<endl; ///TO BE CHANGED!!!
//
writeW << "\nMasses \n"<<endl;
for(int j=1; j<=10;j++) writeW << j << " " << 1 << endl;
 //
int cc=1;
writeW << "\nAtoms \n"<<endl;
for(int nn=0;nn<npoly; nn++){
    for(int m=0;m<nbeads; m++){
        writeW << cc << " " << nn+1 << " " << 1 << " " << position[nn][m][0]<<" " << position[nn][m][1] << " " << position[nn][m][2] << " " << 0 << " " << 0 << " " << 0 << endl;
        cc++;
    }
}
    
//READ AND WRITE AFMBEADS
        
ifstream read;
read.open("map.dat");
read >> afmbeads;
int id, mol, type;
double x,y,z;
int ix,iy,iz;
for(int nn=0;nn<afmbeads; nn++){
    read >> id >> mol >> type >> x >> y >> z >> ix >> iy >> iz;
    writeW << id << " " << mol << " " << type << " " << x <<" " << y << " " << z << " " << ix << " " << iy << " " << iz <<endl;
}
///////////////////////////////////////////////////////////
//FINISHED POLYMER
///////////////////////////////////////////////////////////

writeW << endl;
writeW << endl;
writeW << "\n Velocities \n" <<endl;
for(int j=0;j<totb+afmbeads; j++) writeW<<j+1 << " "<<0 << " "<<0<<" "<<0 <<endl;

writeW << endl;
writeW << "\n Bonds \n"<<endl;
for(int i=0;i<nbond;i++) {
writeW << i+1 <<" "<< 1 <<" "<< bond[i][0]+1<<" "<<bond[i][1]+1 << endl;
}

writeW << "\n Angles \n"<<endl;
for(int i=0;i<nangle;i++) writeW << i+1 <<" "<< 1 <<" "<< angle[i][0]+1<<" "<<angle[i][1]+1 <<" "<< angle[i][2]+1<< endl;

writeW.close();

return 0;
}


