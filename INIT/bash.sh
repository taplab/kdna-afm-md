#! /bin/bash

file=matrix_w_intensity.txt

nc=$(head -n 1 ${file} | wc -w)
nr=$(cat ${file}  | wc -l)
#mean=45 #from EVALUATE_AFM_NOISE
#stdev=5

#PARAMETERS -- DEPEND ON THE IMAGE - NEED HUMAN INPUT!!
nmppx=10 #nm per pixel
length_ring_bp=2500 #length of the rings in bp
#DNA density is ~50rings/um^2
nrings=90 #number of rings in image (1um x 1um crop inside the cap)
DNAwidth=10 #DNA width in nm
size_box=nr #size of the image in number of pixels (taken to be number of rows)

c++ MapMatrix.c++ -o MapMatrix
echo "./MapMatrix_w_intensity ${nr} ${nc} ${nmppx} ${nrings} ${length_ring_bp} ${DNAwidth} ${file}"
./MapMatrix_w_intensity ${nr} ${nc} ${nmppx} ${nrings} ${length_ring_bp} ${DNAwidth} ${file}

nafmpixels=$(head -n 1 map.dat)

c++ Gen_Poly_Rings.c++ -o Gen_Poly_Rings
echo "./Gen_Poly_Rings ${nrings} ${length_ring_bp} ${nr} ${nmppx} ${nafmpixels} ${DNAwidth}"
./Gen_Poly_Rings ${nrings} ${length_ring_bp} ${nr} ${nmppx} ${nafmpixels} ${DNAwidth}

cp minicircles.afm.data kdna.afm.data
